import numpy as np
from typing import Optional
from ShotMaster.constants import BALL_RADIUS, ORIGIN

def distance_to_collision(ray, check_ball) -> Optional[float]:
    """Distance (length of perpendicular segment) from ray trajectory line to checked ball center."""
    if ray.ball is check_ball:
        return None

    start = ray.ball.pos
    end = start + ray.vector
    ball = check_ball.pos
    p = end  - start
    q = ball - start
    t = p.dot(q) / p.dot(p)

    if np.linalg.norm(q - t * p) < 2 * BALL_RADIUS:
        return t
    else:
        return None

def vertex_angle(left, vertex, right) -> float:
    """Angle between 3 points in radians."""
    p = vertex - left
    q = vertex - right

    cosine_angle = np.dot(p, q) / (np.linalg.norm(p) * np.linalg.norm(q))
    angle = np.arccos(cosine_angle)
    return angle

def unit_bisect(vec1: np.array, vec2: np.array) -> np.array:
    """Normalized bisection vector (between two vectors)."""
    vec1 = vec1 / np.linalg.norm(vec1)
    vec2 = vec2 / np.linalg.norm(vec2)
    bisector = vec1 + vec2
    return bisector / np.linalg.norm(bisector)

def distance(start, end):
    return np.linalg.norm(start - end)

def get_perpendicular_vector(vec: np.array) -> np.array:
    x, y = vec
    perp = np.zeros_like(vec)
    perp[:] = [-y, x]
    return perp

def normalize(vec: np.array) -> np.array:
    return vec / np.linalg.norm(vec)

def distnace_to_ball_scaled(vec: np.array, scale) -> float:
    return np.linalg.norm(vec) / scale

def translate(pos: np.array, inv=False) -> np.array:
    """Transition between screen and table corrdinate systems."""
    return np.array(pos)-ORIGIN if inv else tuple(ORIGIN + pos)

pallete = {
    'selection': '#ae00ff',
    'ray': '#FF1493',
    'white': '#ffffff',
    'black': '#000000',
    'half': '#FF0000',
    'full': '#ffff00'
}
