import numpy as np
from dataclasses import dataclass

@dataclass
class Ball(object):
    id: int
    pos: np.ndarray
    
    def get_color(self):
        if self.id == 0:
            return 'white'
        if 1 <= self.id <= 7:
            return 'half'
        if self.id == 8:
            return 'black'
        if 9 <= self.id <= 15:
            return 'full'