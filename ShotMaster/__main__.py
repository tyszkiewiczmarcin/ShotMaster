from ShotMaster.ui import UI
import argparse
from pathlib import Path

parser = argparse.ArgumentParser()
parser.add_argument("--data_dir", type=str, default='/Users/marcin/Dev/Balls/assets/tables/', 
                    help="Path to the tables data directory",)
parser.add_argument('--gen_mode', type=str, default='random',
                    help=f'How balls are generated: random / file')
args = parser.parse_args()

def main() -> None:
    UI(args.data_dir, args.gen_mode).run()

if __name__ == '__main__': 
    main()
