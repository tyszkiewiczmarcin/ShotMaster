from typing import List, Tuple
from ShotMaster.ray import Ray

Shot = List[Ray]
ColorRGB = Tuple[int, int ,int]