from typing import List, Tuple
from ShotMaster.ball import Ball
from ShotMaster.constants import *
from ShotMaster import utils
import numpy as np
import json
import os

class Scene(object):
    index: int=0
    files: List[str]
    
    def __init__(self, mode, data_dir) -> None:
        self.files = [f for f in os.listdir(data_dir) if '.json' in f and 'camera0' in f]
        self.table_dir = data_dir
        self.mode = mode
        
    def next(self) -> None:
        if self.index < len(self.files):
            self.index += 1
    
    def prev(self) -> None:
        if self.index > 0:
            self.index -= 1
            
    def load_balls(self):
        if self.mode == 'random':
            return self._generate_balls()
        if self.mode == 'file':
            return self._load_balls()
            
    def _load_balls(self):
        filepath = self.table_dir + self.files[self.index]
        with open(filepath) as f:
            data = json.load(f)
            
        white = Ball(0, np.array([TABLE_WIDTH//2, TABLE_LENGTH//2]))
        balls: List[Ball]=[white]
                          
        try:
            balls_data = data['balls']
        except:
            return balls
        
        for key, values in balls_data.items():
            color_id = int(key[-2:])
            x = (values['tabley']+1)/2 * TABLE_WIDTH
            y = (values['tablex']+1)/2 * TABLE_LENGTH
            pos = np.array([x, y])
            ball = Ball(color_id, pos)
            
            if ball.get_color() == 'white':
                balls[0] = ball
            else:
                balls.append(ball)
            
        return balls
            
    def _generate_balls(self) -> List[Ball]:
        positions = []
        np.random.seed(self.index)
        while len(positions) < 16:
            x = np.random.uniform(BALL_RADIUS, TABLE_WIDTH - BALL_RADIUS)
            y = np.random.uniform(BALL_RADIUS, TABLE_LENGTH - BALL_RADIUS)
            
            if all(utils.distance(pos, [x, y]) > 2 * BALL_RADIUS for pos in positions):
                positions.append(np.array([x, y]))
                
        return [Ball(id, pos) for id, pos in enumerate(positions)]
    
    