import numpy as np
from typing import List
from ShotMaster.aiming import Shot
import numpy as np


def sort_by_difficulty(shots: List[Shot]) -> List[Shot]:
    return sorted(shots, key=difficulty)

def difficulty(shot: Shot) -> float:
    return np.sum([ray.difficulty() for ray in shot]) * 2