import pygame
import numpy as np
from ShotMaster.constants import *
from ShotMaster.utils import normalize, translate, pallete
from ShotMaster.pocket import generate_pockets, Pocket
from ShotMaster.aiming import search_shots, Shot
from ShotMaster.difficulty import difficulty, sort_by_difficulty
from ShotMaster.scene import Scene
from ShotMaster.typing import ColorRGB
from ShotMaster.constants import *
from colorsys import hsv_to_rgb
from typing import Any, List


class UI:
    running: bool
    screen: Any
    table_surface: Any
    pockets: List[Pocket]

    def __init__(self, data_dir: str, gen_mode: str) -> None:
        pygame.init()
        pygame.display.set_caption('Shot Master 3500')
        self.screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_LENGTH))
        self.table_surface = pygame.image.load('./assets/table.png')
        self.table_surface = pygame.transform.scale(self.table_surface, (SCREEN_WIDTH, SCREEN_LENGTH))
        self.font = pygame.font.SysFont('menlo', int(SCALE * 4))
        self.running = True
        
        self.scene = Scene(mode=gen_mode, data_dir=data_dir)
        self.balls = self.scene.load_balls()
        self.pockets = generate_pockets()
        # self.seed = 100

    # @property
    # def seed(self) -> int:
    #     return self._seed
    
    # @seed.setter
    # def seed(self, value: int) -> None:
    #     self._seed = value
    #     np.random.seed(self.seed)
    #     self.balls = self.scene.load_balls()
    #     self._search_shots()
    
    def _search_shots(self) -> None:
        self.shots = sort_by_difficulty(search_shots(self.balls, self.pockets, depth=DEPTH))
    
    def _handle_events(self) -> None:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.running = False
                
            if event.type == pygame.MOUSEBUTTONDOWN:
                if event.button in (4, 5):
                    if event.button == 4:
                        self.scene.next()
                        
                    if event.button == 5:
                        self.scene.prev()
                        
                self.balls = self.scene.load_balls()
                        
    
    def _move_cue_ball_to_cursor(self) -> None:
        self.balls[0].pos = translate(np.array(pygame.mouse.get_pos()), inv=True)
            
    def _move_cue_ball_auto(self) -> None:
        t = pygame.time.get_ticks()
        scale = 1.5 / (3 - np.cos(2*t))
        x = scale * np.sin(2*t) / 2
        y = scale * np.cos(t)
        self.balls[0].pos = np.array([x, y])
    
    def _get_difficulty_color(self, difficulty: float, max_difficulty: float):
        scale = 0.5 / max_difficulty
        r, g, b = hsv_to_rgb(0.5 - difficulty * scale, 1, 1)
        
        r = int(255*r)
        g = int(255*g)
        b = int(255*b)
        return (r, g, b)

    def _draw_background(self) -> None:
        self.screen.blit(self.table_surface, (0, 0))
    
    def _draw_ball(self, ball) -> None:
        color = pallete[ball.get_color()]
        pygame.draw.circle(self.screen, color, translate(ball.pos), BALL_RADIUS)
        text_color = '#ffffff' if ball.get_color() == 'black' else '#000000'
        text = self.font.render(f'{ball.id}', False, text_color)
        text_rect = text.get_rect()
        self.screen.blit(text, translate(ball.pos-[text_rect.width//2, text_rect.height//2]))
    
    def _draw_balls(self) -> None:
        for ball in self.balls:
            self._draw_ball(ball)
    
    def _draw_shot(self, shot: Shot, color: ColorRGB) -> None:
        for ray in shot:
            ray_start = ray.ball.pos
            ray_end = ray.ball.pos + ray.vector
            line_start = ray_start + normalize(ray.vector) * BALL_RADIUS
            line_end = ray_end - normalize(ray.vector) * BALL_RADIUS
            pygame.draw.circle(self.screen, color, translate(ray_start), BALL_RADIUS, 2)
            pygame.draw.circle(self.screen, color, translate(ray_end), BALL_RADIUS, 2)
            pygame.draw.line(self.screen, color, translate(line_start), translate(line_end))

    def _draw_shots(self, shots: List[Shot]):
        max_difficulty = difficulty(shots[-1])
        for shot in shots:
            self._draw_shot(shot, self._get_difficulty_color(difficulty(shot), max_difficulty))
        
    def _print_shots(self, shots: List[Shot]):
        for shot in shots:
            print(', '.join(f'{shot[i+1].ball.id:>2} :: {ray.print()}' for i, ray in enumerate(shot[:-1])), f'TOTAL :: {difficulty(shot):.3f}')
        print()
    
    def _draw_pocket_ends(self) -> None:
        for pocket in self.pockets:
            pygame.draw.circle(self.screen, '#FF0000', translate(pocket.left), 2)
            pygame.draw.circle(self.screen, '#FF0000', translate(pocket.right), 2)

    def run(self) -> None:
        while self.running:
            
            self._draw_background()
            self._draw_pocket_ends()
            self._handle_events()
            self._move_cue_ball_to_cursor()
            self._draw_balls()
            
            if not pygame.mouse.get_pressed()[0]:
                continue
            self._search_shots()
            
            if len(self.shots) == 0:
                continue
            self._draw_shots(self.shots[:N_SHOTS])
            # self._print_shots(self.shots)
            
            pygame.display.update()
                