import numpy as np
from dataclasses import dataclass
from ShotMaster.ball import Ball
from ShotMaster.utils import distnace_to_ball_scaled
from ShotMaster.constants import TABLE_LENGTH

@dataclass
class Ray:
    ball: Ball
    vector: np.array
    angle: float
    
    def angle_loss(self):
        angle_norm = 2 * self.angle / np.pi - 1
        if angle_norm < 0:
            return 0
        return np.pi / (30 * angle_norm)
        # return 2.718281828 ** (-6*self.angle/np.pi + np.pi)
        # return angle_loss = np.sqrt(2 - np.degrees(self.angle)) / 90
    
    def distance_loss(self):
        return distnace_to_ball_scaled(self.vector, scale=TABLE_LENGTH) / 4
    
    def difficulty(self):
        # return (self.dist_loss + self.angle_loss)**2
        return self.angle_loss()
    
    def print(self):
        return f'<angle_loss :: {self.angle_loss():.3f}, dist_loss :: {self.distance_loss():.3f}, total_loss :: {self.difficulty():.3f}>'
