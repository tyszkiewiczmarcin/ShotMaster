import numpy as np
from dataclasses import dataclass
from typing import List
from ShotMaster.constants import POCKET_RADIUS, TABLE_WIDTH, TABLE_LENGTH

@dataclass
class Pocket:
    is_corner: bool
    left: np.array
    right: np.array
    
def generate_pockets() -> List[Pocket]:
    ψ = 1.3
    return [
        Pocket(is_corner=True, left=(np.array([0, 0]) + [ψ*POCKET_RADIUS, 0]), right=(np.array([0, 0]) + [0, ψ*POCKET_RADIUS])),
        Pocket(is_corner=True, left=(np.array([0, TABLE_LENGTH]) + [ψ*POCKET_RADIUS, 0]), right=(np.array([0, TABLE_LENGTH]) - [0, ψ*POCKET_RADIUS])),
        Pocket(is_corner=True, left=(np.array([TABLE_WIDTH, 0]) - [ψ*POCKET_RADIUS, 0]), right=(np.array([TABLE_WIDTH, 0]) + [0, ψ*POCKET_RADIUS])),
        Pocket(is_corner=True, left=(np.array([TABLE_WIDTH, TABLE_LENGTH]) - [ψ*POCKET_RADIUS, 0]), right=(np.array([TABLE_WIDTH, TABLE_LENGTH]) - [0, ψ*POCKET_RADIUS])),
        Pocket(is_corner=False, left=(np.array([0, TABLE_LENGTH//2]) - [0, POCKET_RADIUS]), right=(np.array([0, TABLE_LENGTH//2]) + [0, POCKET_RADIUS])),
        Pocket(is_corner=False, left=(np.array([TABLE_WIDTH, TABLE_LENGTH//2]) - [0, POCKET_RADIUS]), right=(np.array([TABLE_WIDTH, TABLE_LENGTH//2]) + [0, POCKET_RADIUS]))
        ]