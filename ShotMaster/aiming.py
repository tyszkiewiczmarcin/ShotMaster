from typing import List, DefaultDict, Optional
from collections import defaultdict
import numpy as np
from ShotMaster.ray import Ray
from ShotMaster.ball import Ball
from ShotMaster.pocket import Pocket
from ShotMaster.utils import vertex_angle, unit_bisect, normalize, get_perpendicular_vector, distance_to_collision
from ShotMaster.constants import *
from ShotMaster.typing import Shot

def any_collision(ray: Ray, target_ball: Ball, balls: List[Ball]) -> bool:
    safe_distance = np.linalg.norm(ray.vector)

    for ball in balls:
        if ball is target_ball:
            continue
        
        ball2ball = ball.pos - ray.ball.pos
        
        if ray.vector.dot(ball2ball) < 0:
            # behind
            continue

        dist = distance_to_collision(ray, ball)

        if dist is None:
            continue

        if dist > safe_distance:
            continue
        else:
            return True

    return False

def _ray_to_pocket_bisect(ball: Ball, pocket: Pocket) -> Ray:
    # TODO: środek do przeciwległoego rogu
    vl = pocket.left - ball.pos
    vr = pocket.right - ball.pos
    # triangle side lenghts
    a = np.linalg.norm(pocket.left - pocket.right)
    b = np.linalg.norm(pocket.right - ball.pos)
    c = np.linalg.norm(pocket.left  - ball.pos)
    # bisector length
    d = np.sqrt((b*c)/(b+c)**2 * ((b+c)**2 - a**2))
    # vector to target
    v = d * unit_bisect(vl, vr)
    return Ray(ball, v, vertex_angle(pocket.left, ball.pos, pocket.right))

def is_deathzoned(ray: Ray, pocket: Pocket) -> bool:
    if pocket.is_corner:
        return False
    perp = normalize(get_perpendicular_vector(ray.vector))
    pocket_window_width = np.abs(perp.dot(pocket.left - pocket.right))
    return pocket_window_width <= BALL_RADIUS

def get_pocket_ray(ball: Ball, pocket: Pocket) -> Optional[Ray]:
    ray = _ray_to_pocket_bisect(ball, pocket)

    if is_deathzoned(ray, pocket):
        return None
    else:
        return ray

def get_next_ray(ball: Ball, prev_ray: Ray) -> Optional[Ray]:
    target = prev_ray.ball.pos - 2 * BALL_RADIUS * normalize(prev_ray.vector)
    vec = target - ball.pos
    prev_target = prev_ray.ball.pos + prev_ray.vector
    
    if vec.dot(prev_ray.vector) < 0:
        return None
    return Ray(ball, vec, angle=vertex_angle(ball.pos, target, prev_target))

def search_shots(balls: List[Ball], pockets: List[Pocket], depth: int=2) -> List[Shot]:
    shots: DefaultDict[int, List[Shot]] = defaultdict(list)
    cue_ball = balls[0]

    # W = 0
    for ball in balls:
        if ball is cue_ball:
            continue
        for pocket in pockets:
            ray = get_pocket_ray(ball, pocket)
            if ray is None:
                continue
            if any_collision(ray, None, balls):
                continue
            shots[0].append([ray])
    
    # W > 0
    for w in range(1, depth):
        for shot in shots[w-1]:
            for ball in balls:
                if ball is cue_ball:
                    continue
                ray = get_next_ray(ball, prev_ray=shot[0])
                if ray is None:
                    continue
                if any_collision(ray, balls=balls, target_ball=shot[0].ball):
                    continue
                shots[w].append([ray, *shot])

    # Finish with the cue ball
    shots_flat = []
    for w in range(0, depth):
        for shot in shots[w]:
            ray = get_next_ray(cue_ball, prev_ray=shot[0])
            if ray is None:
                continue
            if any_collision(ray, balls=balls, target_ball=shot[0].ball):
                continue
            shots_flat.append([ray, *shot])
    
    return shots_flat
